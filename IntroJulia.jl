### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ d76ba5a6-ba20-430d-adb8-b5f09ca38da7
md"""
# Intro to Julia

REPL, Command-line Options

## Variables

- Variable names are case-sensitive, and have no semantic meaning
- Unicode names (in UTF-8 encoding) are allowed: δ, α̂⁽²⁾
  - you can type many Unicode math symbols by typing the backslashed LaTeX symbol name followed by tab. E.g. \delta-tab, or even α̂⁽²⁾ by \alpha-tab-\hat- tab-\^(2)-tab.
- If you try to redefine a built-in constant or function already in use, Julia will give you an error.

### Stylistic Conventions

- Names of variables are in **lower case**.
- Word separation can be indicated by underscores ('_'), but use of **underscores is discouraged unless the name would be hard to read otherwise**.
- Names of Types and Modules begin with a capital letter and word separation is shown with upper camel case instead of underscores.
- Names of functions and macros are in lower case, without underscores.
- ??Functions that write to their arguments have names that end in !. These are sometimes called "mutating" or "in-place" functions because they are intended to produce changes in their arguments after the function is called, not just return a value.

Link: [Style Guide](https://docs.julialang.org/en/v1/manual/style-guide/#Style-Guide)

## Integers and Floating-Point Numbers

...

### Numeric Literal Coefficients

Julia allows variables to be immediately preceded by a numeric literal, implying multiplication. This makes writing polynomial expressions much cleaner:
"""

# ╔═╡ cb6b4be6-0995-454b-a356-7b078d396a14
x = 3

# ╔═╡ 75f3c0ed-586f-4813-84f3-ee6ada1d08b9
2x^2 - 3x + 1

# ╔═╡ d850b7f9-ed17-4a1c-ac97-6984d43be622
1.5x^2 - .5x + 1

# ╔═╡ 3790b98f-8674-4a7e-822d-4184401b0d99
2^2x

# ╔═╡ a54a6650-239b-4534-bad0-06f4e1652c55
2(x-1)^2 - 3(x-1) + 1

# ╔═╡ e5639b70-36d3-4746-8212-1106bb458544
(x-1)(x+1)

# ╔═╡ 457cce7e-f3ab-4f79-8666-3bf3362f45fb
x(x+1)

# ╔═╡ b2eef7f7-c0f7-4b16-843f-31669a2aedee
md"""## Mathematical Operations and Elementary Functions

[...](https://docs.julialang.org/en/v1/manual/mathematical-operations/)
"""


# ╔═╡ 0786cf50-8e28-4038-bfce-7e4f3ade558f
NaN * false

# ╔═╡ f9c73718-9d96-4c09-8c75-9cdeef62cc77
false * Inf

# ╔═╡ 6a61102f-e77f-4a46-a9ad-8fd973f274de
md"""
This is useful for preventing the propagation of NaN values in quantities that are known to be zero.
"""

# ╔═╡ 9bcc3e35-be2e-49e2-8a5e-ce09a05d0350
md""" ### Vectorized "dot" operators
 """

# ╔═╡ b62aabea-c836-44e8-9812-0a56e9539316
[1,2,3] ^ 3 # not defined, as it has no meaning!

# ╔═╡ 5b8eb4be-f43b-47a7-ab55-19ed22529361
 [1,2,3] .^ 3 # is parsed as the "dot" call (^).(a,b), which performs a broadcast operation

# ╔═╡ 40f17046-e361-41ec-b29b-f0eedd1e911c
NaN == NaN

# ╔═╡ 9d8c236c-d089-46e1-a4cd-13beb21ccec6
NaN != NaN # NB

# ╔═╡ 101f72e3-5d37-43ad-8320-c4d01cf27e4a
NaN < NaN

# ╔═╡ d11541b0-2598-4fa2-8d6f-2ab95f6eb56e
NaN > NaN

# ╔═╡ bb721966-4f87-45e8-8156-ef933064d781
[1 NaN] == [1 NaN] # can cause headaches when working with arrays

# ╔═╡ e1c40505-a8fb-4906-a409-9ad01e8141ca
isequal(NaN, NaN)

# ╔═╡ 4da812ab-af06-4454-9bde-b95dcd33f6d2
isequal([1 NaN], [1 NaN])

# ╔═╡ 97ee5890-3635-4f67-b1b2-a971f8e05c11
isequal(NaN, NaN32)

# ╔═╡ 0f9a6ae7-ad9c-4010-9e8f-416288e9cc3f
-0.0 == 0.0

# ╔═╡ b8c1943a-cad8-4093-b365-3894832c1c3f
isequal(-0.0, 0.0)

# ╔═╡ fbdc09ac-0db2-11ec-2762-e7d0c9522334
md"""
## Complex and Rational Numbers
## Strings

## Functions

A function is an object that maps a tuple of argument values to a return value
"""

# ╔═╡ 360681e7-3ab8-4b63-af2f-461ec2356897
∑(x,y) = x + y

# ╔═╡ ba803005-dc00-46c3-a082-e6f1f5743d67
∑(2, 3)

# ╔═╡ e61b3bbf-93bb-4422-ae42-a6217a0d6bc8
md"""
### Argument Passing [Behavior](https://docs.julialang.org/en/v1/manual/functions/#Argument-Passing-Behavior)
"""

# ╔═╡ 0e769380-2004-4d72-a34a-253fe001bff1
md"""
 """

# ╔═╡ 698dc319-0284-4571-bc55-9922c924230d
function f(x,y)
           x + y
       end

# ╔═╡ 83e433aa-e3ca-4fb8-bcea-7c21d64c5fd8
 f(x,y) = x + y # A more terse syntax for defining a function in Julia.

# ╔═╡ Cell order:
# ╟─d76ba5a6-ba20-430d-adb8-b5f09ca38da7
# ╠═cb6b4be6-0995-454b-a356-7b078d396a14
# ╠═75f3c0ed-586f-4813-84f3-ee6ada1d08b9
# ╠═d850b7f9-ed17-4a1c-ac97-6984d43be622
# ╠═3790b98f-8674-4a7e-822d-4184401b0d99
# ╠═a54a6650-239b-4534-bad0-06f4e1652c55
# ╠═e5639b70-36d3-4746-8212-1106bb458544
# ╠═457cce7e-f3ab-4f79-8666-3bf3362f45fb
# ╠═b2eef7f7-c0f7-4b16-843f-31669a2aedee
# ╠═0786cf50-8e28-4038-bfce-7e4f3ade558f
# ╠═f9c73718-9d96-4c09-8c75-9cdeef62cc77
# ╟─6a61102f-e77f-4a46-a9ad-8fd973f274de
# ╠═9bcc3e35-be2e-49e2-8a5e-ce09a05d0350
# ╠═b62aabea-c836-44e8-9812-0a56e9539316
# ╠═5b8eb4be-f43b-47a7-ab55-19ed22529361
# ╠═40f17046-e361-41ec-b29b-f0eedd1e911c
# ╠═9d8c236c-d089-46e1-a4cd-13beb21ccec6
# ╠═101f72e3-5d37-43ad-8320-c4d01cf27e4a
# ╠═d11541b0-2598-4fa2-8d6f-2ab95f6eb56e
# ╠═bb721966-4f87-45e8-8156-ef933064d781
# ╠═e1c40505-a8fb-4906-a409-9ad01e8141ca
# ╠═4da812ab-af06-4454-9bde-b95dcd33f6d2
# ╠═97ee5890-3635-4f67-b1b2-a971f8e05c11
# ╠═0f9a6ae7-ad9c-4010-9e8f-416288e9cc3f
# ╠═b8c1943a-cad8-4093-b365-3894832c1c3f
# ╟─fbdc09ac-0db2-11ec-2762-e7d0c9522334
# ╠═698dc319-0284-4571-bc55-9922c924230d
# ╠═83e433aa-e3ca-4fb8-bcea-7c21d64c5fd8
# ╠═360681e7-3ab8-4b63-af2f-461ec2356897
# ╠═ba803005-dc00-46c3-a082-e6f1f5743d67
# ╠═e61b3bbf-93bb-4422-ae42-a6217a0d6bc8
# ╠═0e769380-2004-4d72-a34a-253fe001bff1
