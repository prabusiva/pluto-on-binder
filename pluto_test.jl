### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ db8ab638-0cd3-11ec-342c-f55ae7abdf90
1+1

# ╔═╡ b36cf90f-a8cc-4a54-99d5-2a12bf31cdb6
function estimate_pi(n)
    s = 1.0
    for i in 1:n
        s += (isodd(i) ? -1 : 1) / (2i + 1)
    end
    4s
end

# ╔═╡ 3be43d07-b679-441b-b337-d4b734c44743
p = estimate_pi(100_000_000)

# ╔═╡ 83334d4f-85c9-482b-8701-e2ea2a0df48f
"π ≈ $p"

# ╔═╡ 8be31c44-0e21-4062-af43-e832b684f4ee
"Error is $(p - π)"

# ╔═╡ bff9f438-3a8a-4723-a414-58cc03580c61
#using WordCloud
#using Random
#words = [randstring(rand(1:8)) for i in 1:300]
#weights = randexp(length(words))
#wc1 = wordcloud(words, weights)
#generate!(wc1)
##paint(wc1, "random.svg")
#wc2 = wordcloud("It's easy to generate word clouds") |> generate!
#wc3 = wordcloud(open(pkgdir(WordCloud)*"/res/alice.txt")) |> generate!

# ╔═╡ 6407c93a-9431-44b7-9664-355ef038a572
using Plots

# ╔═╡ 4caf821a-ea26-4874-b757-698125489844
1+1

# ╔═╡ 29718aca-b792-4b5d-af61-71b5996aae92
x = 1:10

# ╔═╡ 81cc4b05-0f54-49c6-9590-a5e095ed5bb2
y = rand(10)

# ╔═╡ a959eeda-af12-4f2a-853d-a9d8b8fe59f5
plot(x,y)

# ╔═╡ 9487df9e-669a-49ac-8785-ae32164f5066
a = 1:12; b = rand(12); # These are the plotting data

# ╔═╡ 9a71e86b-2986-45d5-8fd4-0f026018e419
plot(x, y)

# ╔═╡ 2b534719-605a-4980-8cee-b7acb3d3b26c
plot(y,x)

